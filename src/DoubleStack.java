import java.util.*;

/** Stack manipulation.
 * @since 1.8
 */
public class DoubleStack {

   public static void main(String[] argum) {
      String s = "-3 -5 -7 ROT - SWAP DUP * +";
      System.out.println(interpret(s));
   }

   private LinkedList<String> dstack;


   DoubleStack() {
      dstack = new LinkedList<String>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      DoubleStack tmp = new DoubleStack();
      tmp.cloneSetter((LinkedList<String>) this.dstack.clone());
      return tmp;
   }

   private void cloneSetter (LinkedList<String> dstack) {
      this.dstack = dstack;
   }

   public boolean stEmpty() {
      return ((dstack.size()-1) < 0); //Checks if the stack is empty. Dstack size is usually subtracted by one because of previous infrastructure
   }

   public void push (double a) {
      dstack.add(Double.toString(a));
   }

   public double pop() {
      if (stEmpty()){
         throw new IndexOutOfBoundsException("Stack underflow!");
      }
      double tmp = Double.parseDouble(dstack.getLast());
      dstack.removeLast();
      return tmp;
   }

   public void op (String s) {
      if ((dstack.size()-1) < 1){
         throw new IndexOutOfBoundsException("Too few elements for " + s);
      }
      double op2 = pop();
      double op1 = pop();
      if (s.equals("+")) {
         push(op1 + op2);
      } else if (s.equals("-")){
         push(op1 - op2);
      } else if (s.equals("*")){
         push(op1 * op2);
      } else if (s.equals("/")){
         push(op1/op2);
      } else{
         throw new IllegalArgumentException("Invalid operation " + s);
      }
   }

   public void opCustom (String s){
      if (s.equals("SWAP")){
         if ((dstack.size() < 2)){
            throw new IndexOutOfBoundsException("Too few elements for " + s);
         }
         double op2 = pop();
         double op1 = pop();
         push(op2);
         push(op1);
      } else if (s.equals("DUP")){
         if ((dstack.size() < 1)){
            throw new IndexOutOfBoundsException("Too few elements for " + s);
         }
         double op1 = pop();
         push(op1);
         push(op1);
      } else if (s.equals("ROT")){
         if ((dstack.size() < 3)){
            throw new IndexOutOfBoundsException("Too few elements for " + s);
         }
         double op3 = pop();
         double op2 = pop();
         double op1 = pop();
         push(op2);
         push(op3);
         push(op1);
      } else{
         throw new IllegalArgumentException("Invalid operation " + s); //Technically this does not need to be here as the interpreter should manage it
      }
   }


   public double tos() {
      if (stEmpty()){
         throw new IndexOutOfBoundsException("Stack underflow");
      }
      return Double.parseDouble(dstack.getLast());
   }

   @Override
   public boolean equals (Object o) {
      System.out.println("starting equals");
      if (((DoubleStack) o).dstack.size()-1 != (dstack.size()-1)){
         return false;
      }
      if ((((DoubleStack) o).dstack.size()-1) == (dstack.size()-1) && ((dstack.size()-1) == -1)){
         return true; //Basically magic checking for equalities by sizes as well
      }
      Iterator<String> iteratororig = dstack.listIterator();
      Iterator<String> iteratorcomp = ((DoubleStack) o).dstack.listIterator();
      String a = "a";
      String b = "b";
      System.out.println("(dstack.size()-1) is " + (dstack.size()-1));
      for (int i = 0; i <= (dstack.size()-1); i++) {
         a = iteratorcomp.next();
         System.out.println("a is " + a);
         b = iteratororig.next();
         System.out.println("b is " + b);
         if (!a.equals(b)) {
            return false;
         }
      }
      return true;
   }

   @Override
   public String toString() {
      if (stEmpty()) {
         return "empty";
      }
      StringBuffer b = new StringBuffer();
      Iterator<String> iterator = dstack.iterator();
      for (int i = 0; i <= (dstack.size()-1); i++){
         b.append(iterator.next());
      }
      return b.toString();
   }

   public static double interpret (String pol) {
      if (pol == null){
         throw new IllegalArgumentException("Null string fed into RPN interpreter");
      }
      String[] inputStringArr = pol.trim().split("\\s+"); // equivalent to [ \\t\\n\\x0B\\f\\r], splits all whitespaces
      if (inputStringArr.length == 0){
         throw new IllegalArgumentException("Non-meaningful string fed into RPN interpreter");
      }
      DoubleStack istack = new DoubleStack();
      int operandcount = 0;
      int numbercount = 0;
//      String[] operands = {"+", "-", "/", "*"};
      for (String a: inputStringArr){
         System.out.println("Interpreting " + a);
         if (isDouble(a) == true){
            istack.push(Double.parseDouble(a));
            numbercount++;
         } else{
            if (isCustom(a)){
              istack.opCustom(a);
            } else{
               try {
                  istack.op(a);
               } catch(IndexOutOfBoundsException e){
                  throw new IllegalArgumentException("Too few elements for operation in " + pol + ". Make sure there's one more number than operands");
               } catch(IllegalArgumentException e){
                  throw new IllegalArgumentException("Illegal operand found in " + pol);
               }
            }
            operandcount++;
         }
      }
      if (numbercount > operandcount+1){
         throw new IndexOutOfBoundsException("Too many operands in string" + pol);
      } else {
         return istack.pop();
      }
   }

   private static boolean isDouble(String a){ //Checks if string value is equivalent to a double
      try{
         double dVal = Double.parseDouble(a);
         return true;
      } catch(NumberFormatException e) {
      }
      return false;
   }

   private static boolean isCustom(String a){ //Checks for the three new custom strings, built to be easily expandable
      System.out.println("Evaluating " + a);
      if (a.equals("SWAP")){
         System.out.println("returning true");
         return true;
      } else if (a.equals("DUP")){
         System.out.println("returning true");
         return true;
      } else if (a.equals("ROT")){
         System.out.println("returning true");
         return true;
      } else{
         System.out.println("returning false");
         return false;
      }
   }

}

